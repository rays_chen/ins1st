package com.ins1st.modules.sys.role.mapper;

import com.ins1st.modules.sys.role.entity.SysUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户和角色表 Mapper 接口
 * </p>
 *
 * @author sun
 * @since 2019-05-09
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
